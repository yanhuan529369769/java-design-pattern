public class Main{
    public static void main(String[] argv){
        //公交车的价格会随着季节的变化而变化
        BusContext context = new BusContext();

        //此时是春季
        context.setSeason(BusContext.Season.SPRING);
        context.showPrice();

        //此时是夏季
        context.setSeason(BusContext.Season.SUMMER);
        context.showPrice();

        //此时是秋季
        context.setSeason(BusContext.Season.AUTUMN);
        context.showPrice();

        //此时是冬季
        context.setSeason(BusContext.Season.WINTER);
        context.showPrice();
    }
}
