public class Main{
    public static void main(String[] argv){
        // 生成一个签名很慢
        Signature signature = new Md5Signature("Hello", "world2");
        System.out.println(signature.getSignature());

        // 克隆一个签名很快
        Signature signature2 = signature.clone();
        System.out.println(signature2.getSignature());
    }
}
