public class Main{
    public static void main(String[] argv){
        AppleFactory factory = new AppleFactoryImpl();
        iPhone ip4 = factory.createiPhone(AppleFactory.IPHONE_4);
        iPhone ip5 = factory.createiPhone(AppleFactory.IPHONE_5);
        ip4.turnOn();
        ip5.turnOn();
    }
}
