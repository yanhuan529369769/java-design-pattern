public class Main{
    public static void main(String[] argv){
        Database database = new Database();
        database.insert("909090");
        database.show();

        database.beginTransaction();
        try{
            database.insert("aaa");
            database.insert("bbb");
            database.insert("ccc");
            throw new RuntimeException();
        }catch(Exception ex){
            database.rollback();
        }finally{
            database.endTransaction();
        }

        database.show();
    }
}
