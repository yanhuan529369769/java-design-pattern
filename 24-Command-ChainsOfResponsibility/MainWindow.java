public class MainWindow{
    private Handler handler;

    public MainWindow(){
    }

    public void setEventHandler(Handler handler){
        this.handler = handler;
    }

    public void click(){
        this.handler.handle(new MouseDownEvent());
        this.handler.handle(new MouseUpEvent());
        this.handler.handle(new ClickEvent());
    }
}
