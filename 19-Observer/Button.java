public interface Button{
    public void addOnClickListener(OnClickListener listener);
    public void setColor(String color);
    public void setPosition(int x, int y);
    public void click();
}
