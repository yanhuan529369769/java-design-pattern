public class Main{
    public static void main(String[] argv){
        //新建按钮
        final Button button = new ButtonImpl();

        //点击按钮后设置颜色
        button.addOnClickListener(new OnClickListener(){
            @Override
            public void onClick(){
                button.setColor("white");
            }
        });

        //点击按钮后设置位置
        button.addOnClickListener(new OnClickListener(){
            @Override
            public void onClick(){
                button.setPosition(1,2);
            }
        });

        //点击按钮
        button.click();
    }
}
