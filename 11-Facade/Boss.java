public class Boss{
    private Secretary secretary;

    public Boss(Secretary secretary){
        this.secretary = secretary;
    }

    public void run(){
        this.secretary.buyTicket();
        this.secretary.bookRoom();
        this.secretary.eatLunch();
    }
}
