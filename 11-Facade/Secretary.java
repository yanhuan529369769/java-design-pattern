public class Secretary {
    private Airport airport = new Airport();
    private Hotel hotel = new Hotel();
    private Restaurant restaurant = new Restaurant();

    public void buyTicket(){
        airport.buyTicket();
    }

    public void bookRoom(){
        hotel.bookRoom();
    }

    public void eatLunch(){
        restaurant.eatLunch();
    }
}
