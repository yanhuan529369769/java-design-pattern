public interface Expression {
    public double eval(Context context);
}
