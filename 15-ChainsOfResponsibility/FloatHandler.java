public class FloatHandler implements Handler{
    private Handler next;

    public FloatHandler(Handler next){
        this.next = next;
    }

    @Override
    public void handle(String string){
        //转换成小数
        float value;
        try{
            value = Float.parseFloat(string);
        }catch(Exception ex){
            if(this.next != null){
                this.next.handle(string);
            }
            return;
        }

        //处理小数
        System.out.println("FloatHandler: " + value);
    }
}
