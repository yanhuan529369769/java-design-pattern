public interface Window{
    public void addButton(String message);
    public void addInput(String message);
    public void show();
}
