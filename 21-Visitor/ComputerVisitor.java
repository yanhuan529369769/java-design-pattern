public interface ComputerVisitor{
    public void visit(CPU cpu, Harddisk hd);
}
