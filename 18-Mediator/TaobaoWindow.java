public class TaobaoWindow implements Window{
    private Label itemNameLabel;
    private Button buyButton;
    private ItemList itemList;
    
    public TaobaoWindow(){
        //显示商品名称
        itemNameLabel = new LabelImpl(this);

        //显示购买按钮
        buyButton = new ButtonImpl(this);

        //显示商品列表
        itemList = new ItemListImpl(this);
        itemList.add("Apple");
        itemList.add("Banana");
        itemList.add("Car");
    }

    public void test(){
        itemList.selectItem(0);
        itemList.selectItem(1);
        itemList.unselectItem();
    }

    @Override
    public void onSelectItem(String item){
        buyButton.enable();
        itemNameLabel.setText(item);
    }

    @Override
    public void onUnselectItem(){
        buyButton.disable();
        itemNameLabel.setText("请选择商品");
    }
}
