public class Main{
    public static void main(String[] argv){
        Computer computer=new ComputerProxy(new RealComputer());
        computer.startup();
        computer.shutdown();
    }
}
