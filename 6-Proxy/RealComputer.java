public class RealComputer implements Computer{
    @Override
    public void startup(){
        System.out.println("RealComputer: startup");
    }

    @Override
    public void shutdown(){
        System.out.println("RealComputer: shutdown");
    }
}
