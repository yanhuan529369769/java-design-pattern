public class LinkedList<T> implements Iterable<T>{
    private Node root;
    private Node lastNode;

    private class Node{
        public T value;
        public Node next;
    }

    public LinkedList(){
    }

    @Override
    public Iterator<T> getIterator(){
        return new MyIterator();
    }

    public void add(T value){
        //新建节点
        Node node = new Node();
        node.value = value;

        //将节点加入到链表末尾
        if(lastNode == null){
            lastNode = node;
            root = node;
        } else {
            lastNode.next = node;
            lastNode = node;
        }
    }

    private class MyIterator implements Iterator<T> {
        private Node current;

        public MyIterator(){
            current = root;
        }

        @Override
        public boolean hasNext(){
            return current != null;
        }

        @Override
        public T next(){
            T result = current.value;
            this.current = this.current.next;
            return result;
        }
    }
}
