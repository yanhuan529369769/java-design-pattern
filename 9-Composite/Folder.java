import java.util.List;
import java.util.LinkedList;

public class Folder implements Node{
    private String name;
    private List<Node> files = new LinkedList<Node>();

    public Folder(String name){
        this.name = name;
    }

    @Override
    public String getFileName(){
        return name;
    }

    public void addFile(Node node){
        this.files.add(node);
    }

    public void deleteFile(Node node){
        this.files.remove(node);
    }

    public List<Node> getFiles(){
        return files;
    }
}
