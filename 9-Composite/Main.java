public class Main{
    public static void main(String[] arg){
        // 配置目录和文件
        Folder etc = new Folder("etc/");
        File etc_passwd = new File("passwd");
        File etc_shadow = new File("shadow");
        etc_passwd.write("passwd content");
        etc_shadow.write("shadow content");
        etc.addFile(etc_passwd);
        etc.addFile(etc_shadow);

        // bin目录
        Folder bin = new Folder("bin/");
        File bin_bash = new File("bash");
        bin_bash.write("bash content");
        bin.addFile(bin_bash);

        // 根目录中增加etc和bin
        Folder root = new Folder("/");
        root.addFile(etc);
        root.addFile(bin);

        // 显示目录内容
        Main.printFile("", root);
    }

    public static void printFile(String base, Node node){
        // 如果是文件夹，输出文件夹的名称以及包含的文件
        if(node instanceof Folder){
            Folder folder = (Folder)node;
            System.out.println(String.format("Folder: %s%s", base, node.getFileName()));
            for(Node each : folder.getFiles()){
                printFile(base + node.getFileName(), each);
            }
        }

        // 如果是文件，输入文件的名称以及文件内容
        else if(node instanceof File){
            File file = (File)node;
            System.out.println(String.format("File: %s%s Content: %s", base, node.getFileName(), file.read()));
        }

        // 其他情况抛出异常
        else{
            throw new RuntimeException("Unknown node type");
        }
    }
}
