public class Main{
    public static void main(String[] argv){
        //排序整数数据
        Integer[] list = {12,31,23,11,2,33,12,122,1,2,3,4,6,4,3,2,231};
        new IntegerSort().sort(list);

        //输出结果
        for(Integer each : list){
            System.out.println(each);
        }

        //排序字符串数据
        String[] list2 = {"passwd","shadow","did","pock","era"};
        new StringSort().sort(list2);

        //输出结果
        for(String each : list2){
            System.out.println(each);
        }
    }
}
