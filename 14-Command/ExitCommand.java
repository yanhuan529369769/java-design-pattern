public class ExitCommand implements Command {
    private Main receiver;

    public ExitCommand(Main receiver){
        this.receiver = receiver;
    }

    @Override
    public void execute(){
        receiver.onExit();
    }
}
